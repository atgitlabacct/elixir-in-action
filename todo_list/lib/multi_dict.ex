defmodule MultiDict do
  @moduledoc """
    Holds a Dict where the key maps to a list and if the key is given with a value
    the value is added to the list
  """
  @spec new :: map
  def new, do: %{}

  @spec add(Map.t(), Date.t(), any) :: map
  def add(dict, key, value) do
    Map.update(dict, key, [value], &[value | &1])
  end

  @spec get(Map.t(), Date.t()) :: list
  def get(dict, key) do
    Map.get(dict, key, [])
  end
end
