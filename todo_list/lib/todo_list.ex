defmodule TodoList do
  defstruct auto_id: 1, entries: %{}

  @type t :: %TodoList{auto_id: integer, entries: Map.t()}

  @moduledoc """
  A Todolist where the entries are organized by date.
  """

  @doc """
  Creates a new todo list

  ## Examples

    iex> TodoList.new()
    %TodoList{auto_id: 1, entries: %{}}

  """
  @spec new :: TodoList.t()
  def new, do: %TodoList{}

  @doc """
  Create a new todolist given a set of entries

  ## Examples

    iex> entries = [
    ...>  %{date: ~D[2018-12-19], title: "Dentist"},
    ...>  %{date: ~D[2018-12-20], title: "Movies"}
    ...> ]
    iex> %TodoList{
    ...> auto_id: 3,
    ...>   entries: %{
    ...>     1 => %{id: 1, date: ~D[2018-12-19], title: "Dentist"},
    ...>     2 => %{id: 2, date: ~D[2018-12-20], title: "Movies"}
    ...>   }
    ...> } = TodoList.new(entries)
  """
  @spec new(list(map)) :: TodoList.t()
  def new(entries) when is_list(entries) do
    Enum.reduce(entries, TodoList.new(), &add_entry(&2, &1))
  end

  @doc """
  Add an entry to the TodoList

  ## Examples

    iex> TodoList.new()
    ...> |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
    ...> |> TodoList.add_entry(%{date: ~D[2018-12-20], title: "Shopping"})
    ...> |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Movies"})
    %TodoList{
      auto_id: 4,
      entries: %{
        1 => %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
        2 => %{date: ~D[2018-12-20], id: 2, title: "Shopping"},
        3 => %{date: ~D[2018-12-19], id: 3, title: "Movies"}
      }
    }

  """
  @spec add_entry(TodoList.t(), map) :: TodoList.t()
  def add_entry(todo_list, entry) do
    entry = Map.put(entry, :id, todo_list.auto_id)

    new_entries =
      Map.put(
        todo_list.entries,
        todo_list.auto_id,
        entry
      )

    %TodoList{todo_list | entries: new_entries, auto_id: todo_list.auto_id + 1}
  end

  @doc """
  Return a list of entries

  ## Examples

    iex> todo_list =
    ...>  TodoList.new()
    ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
    ...>  |> TodoList.add_entry(%{date: ~D[2018-12-20], title: "Shopping"})
    ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Movies"})
    iex> TodoList.entries(todo_list, ~D[2018-12-19])
    [
       %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
       %{date: ~D[2018-12-19], id: 3, title: "Movies"}
    ]
    iex> TodoList.entries(todo_list, ~D[2000-01-01])
    []

  """
  @spec entries(TodoList.t(), Date.t()) :: [map]
  def entries(todo_list, date) do
    todo_list.entries
    |> Stream.filter(fn {_, entry} -> entry.date == date end)
    |> Enum.map(fn {_, entry} -> entry end)
  end

  @doc """
  Update an entry in the todolist

  ## Examples

    iex> todo_list =
    ...> TodoList.new()
    ...> |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Movies"})
    ...> |> TodoList.update_entry(%{id: 1, date: ~D[2018-12-20], title: "Movies"})
    iex> TodoList.entries(todo_list, ~D[2018-12-20])
    [
       %{date: ~D[2018-12-20], id: 1, title: "Movies"}
    ]
    iex> TodoList.entries(todo_list, ~D[2018-12-19])
    []

  """
  @spec update_entry(TodoList.t(), map) :: TodoList.t()
  def update_entry(%TodoList{} = todo_list, %{id: id} = new_entry) do
    update_entry(todo_list, id, fn old_entry ->
      Map.merge(old_entry, new_entry)
    end)
  end

  @doc """
  Update an entry in the todolist

  ## Examples

    iex> TodoList.new()
    ...> |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Movies"})
    ...> |> TodoList.update_entry(
    ...>    1,
    ...>    &Map.put(&1, :date, ~D[2018-12-20])
    ...>  )
    %TodoList{
      auto_id: 2,
      entries: %{
        1 => %{id: 1, date: ~D[2018-12-20], title: "Movies"}
      }
    }

  """
  @spec update_entry(TodoList.t(), integer, function) :: TodoList.t()
  def update_entry(todo_list, entry_id, update_fun) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list

      {:ok, old_entry} ->
        old_entry_id = old_entry.id
        new_entry = %{id: ^old_entry_id} = update_fun.(old_entry)
        new_entries = Map.put(todo_list.entries, new_entry.id, new_entry)
        %TodoList{todo_list | entries: new_entries}
    end
  end

  @doc """
  Remove an entry from the todo list

  ## Examples

    iex> todo_list = TodoList.new([%{date: ~D[2018-12-19], title: "Movies"}])
    iex> TodoList.remove_entry(todo_list, 1)
    %TodoList{auto_id: 2, entries: %{}}
  """
  @spec remove_entry(TodoList.t(), integer) :: TodoList.t()
  def remove_entry(todo_list, entry_id) do
    new_entries = Map.delete(todo_list.entries, entry_id)
    %TodoList{todo_list | entries: new_entries}
  end
end
