defmodule Fraction do
  @moduledoc """
  Fraction module
  """
  defstruct a: nil, b: nil

  @type t :: %Fraction{a: integer, b: integer}

  @spec new(integer, integer) :: Fraction.t()
  def new(numerator, denominator), do: %Fraction{a: numerator, b: denominator}

  def numerator(%Fraction{} = fraction), do: fraction.a

  def denominator(%Fraction{} = fraction), do: fraction.b

  def value(%Fraction{} = fraction) do
    Fraction.numerator(fraction) / Fraction.denominator(fraction)
  end

  @doc """
  Adds to fractions together

  ## Examples

    iex> f1 = Fraction.new(1, 2)
    iex> f2 = Fraction.new(1, 2)
    iex> Fraction.add(f1, f2)
    %Fraction{a: 2, b: 2}
  """
  def add(%Fraction{a: num1, b: dem1}, %Fraction{a: num2, b: dem2}) do
    new(
      num1 * dem2,
      num2 * dem1
    )
  end
end
