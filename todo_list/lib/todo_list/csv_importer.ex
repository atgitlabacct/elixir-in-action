defmodule TodoList.CsvImporter do
  @moduledoc """
  A Todolist where the entries are organized by date.
  """

  @doc """
  Creates a TodoList from a csv file

  ## Examples

    iex> {:ok, csv_path} = File.cwd()
    iex> csv_path
    ...> |> Path.join("todos.csv")
    ...> |> TodoList.CsvImporter.load
    %TodoList{
      auto_id: 4,
      entries: %{
        1 => %{id: 1, date: ~D[2018-12-19], title: "Dentist"},
        2 => %{id: 2, date: ~D[2018-12-20], title: "Shopping"},
        3 => %{id: 3, date: ~D[2018-12-19], title: "Movies"}
      }
    }

  """
  @spec load(String.t()) :: TodoList.t()
  def load(path) when is_binary(path) do
    entries =
      File.stream!(path, [:utf8])
      |> Stream.map(&String.replace(&1, "\n", ""))
      |> Stream.map(&String.split(&1, ","))
      |> Stream.map(fn [date, title] ->
        [year, month, day] = String.split(date, "/")

        {:ok, date} =
          Date.new(
            String.to_integer(year),
            String.to_integer(month),
            String.to_integer(day)
          )

        %{title: title, date: date}
      end)
      |> Enum.to_list()

    TodoList.new(entries)
  end
end
